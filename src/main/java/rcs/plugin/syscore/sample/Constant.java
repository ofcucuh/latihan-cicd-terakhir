/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rcs.plugin.syscore.sample;

import java.util.ArrayList;

/**
 *
 * @author Gradi
 */
public class Constant {

    public static final String cModuleNamespace = "sample";
    public static final String cFieldMTI = "MTI";
    public static final String cFieldPAN = "PAN";
    public static final String cFieldPC = "PC";
    public static final String cFieldRC = "RC";
    public static final String cFieldRCM = "RCM";
    public static final String cConfValidProducts = "valid-products";
    public static final String cPCHello = "380001";
    public static final ArrayList<Object> cDefaultPAN = getDefaultPAN();

    protected static final ArrayList<Object> getDefaultPAN() {
        ArrayList<Object> tList = new ArrayList<Object>();
        tList.add("10000");
        return tList;
    }
}
